package com.jeffreyj100.recall;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.test.AndroidTestCase;
import android.util.Log;

import com.jeffreyj100.recall.database.MyDbHelper;

import java.util.Map;
import java.util.Set;

public class TestDatabase extends AndroidTestCase {
    private static final String LOG_TAG = TestDatabase.class.getSimpleName();

    private static final String SUBJECT_1_TITLE = "Information Assurance";
    private static final String SUBJECT_2_TITLE = "Programming Languages";

    private static final String DECK_1_TITLE = "Authentication Service"; // (Subject 1)
    private static final String DECK_2_TITLE = "Network Vulnerabilities"; // (Subject 1)

    // Flashcard 1 (Subject 1 - Deck 1)
    private static final String FLASHCARD_1_TITLE = DECK_1_TITLE;
    private static final String FLASHCARD_1_QUESTION = "What is an example of authentication services?";
    private static final String FLASHCARD_1_ANSWER = "RADIUS";

    // Flashcard 2 (Subject 1 - Deck 1)
    private static final String FLASHCARD_2_TITLE = DECK_1_TITLE;
    private static final String FLASHCARD_2_QUESTION = "What is a better implementation of RADIUS?";
    private static final String FLASHCARD_2_ANSWER = "DIAMETER";

    // Flashcard 3 (Subject 1 - Deck 2)
    private static final String FLASHCARD_3_TITLE = DECK_2_TITLE;
    private static final String FLASHCARD_3_QUESTION = "An example of a penetration testing methodology?";
    private static final String FLASHCARD_3_ANSWER = "OSSTMM";

    // DeckStatistic (Deck 1)
    private static final String DECK_STATISTIC_1_TITLE = DECK_1_TITLE;
    private static final float DECK_STATISTIC_1_SCORE = (float) 0.93;
    private static final double DECK_STATISTIC_1_TIME_SPENT = 34.0;

    public void testCreateDb() throws Throwable {
        mContext.deleteDatabase(MyDbHelper.DATABASE_NAME);
        SQLiteDatabase db = new MyDbHelper(this.mContext).getWritableDatabase();
        assertEquals(true, db.isOpen());
        db.close();
    }

    public void testInsertReadSubject() {
        MyDbHelper dbHelper = new MyDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Subject 1
        ContentValues testSubjectValues1 = new ContentValues();
        testSubjectValues1.put(MyDbHelper.TITLE, SUBJECT_1_TITLE);

        long locationRowId;
        locationRowId = db.insert(MyDbHelper.SUBJECTS_TABLE, null, testSubjectValues1);

        // Verify row
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        // Query results
        Cursor cursor = db.query(
                MyDbHelper.SUBJECTS_TABLE,
                null, // All columns
                "id = ?", // Columns for where clause
                new String[] {"1"}, // Values for where clause
                null, // Columns to group by
                null, // Columns to filter by row groups
                null // sort order
        );

        verifyCursor(testSubjectValues1, cursor);

        // Subject 2
        ContentValues testSubjectValues2 = new ContentValues();
        testSubjectValues2.put(MyDbHelper.TITLE, SUBJECT_2_TITLE);

        locationRowId = db.insert(MyDbHelper.SUBJECTS_TABLE, null, testSubjectValues2);

        // Verify row
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        // Query results
        cursor = db.query(
                MyDbHelper.SUBJECTS_TABLE,
                null, // All columns
                "id = ?", // Columns for where clause
                new String[] {"2"}, // Values for where clause
                null, // Columns to group by
                null, // Columns to filter by row groups
                null // sort order
        );

        verifyCursor(testSubjectValues2, cursor);

        dbHelper.close();
    }

    public void testInsertReadDeck() {
        MyDbHelper dbHelper = new MyDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Deck 1 (Subject 1)
        ContentValues testDeckValues1 = new ContentValues();
        testDeckValues1.put(MyDbHelper.TITLE, DECK_1_TITLE);
        testDeckValues1.put(MyDbHelper.BELONGS_TO, 1);

        long locationRowId;
        locationRowId = db.insert(MyDbHelper.DECKS_TABLE, null, testDeckValues1);

        // Verify row
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        // Query results
        Cursor cursor = db.query(
                MyDbHelper.DECKS_TABLE,
                null, // All columns
                "id = ?", // Columns for where clause
                new String[] {"1"}, // Values for where clause
                null, // Columns to group by
                null, // Columns to filter by row groups
                null // sort order
        );

        verifyCursor(testDeckValues1, cursor);

        // Deck 2 (Subject 1)
        ContentValues testDeckValues2 = new ContentValues();
        testDeckValues2.put(MyDbHelper.TITLE, DECK_2_TITLE);
        testDeckValues2.put(MyDbHelper.BELONGS_TO, 1);

        locationRowId = db.insert(MyDbHelper.DECKS_TABLE, null, testDeckValues2);

        // Verify row
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        // Query results
        cursor = db.query(
                MyDbHelper.DECKS_TABLE,
                null, // All columns
                "id = ?", // Columns for where clause
                new String[] {"2"}, // Values for where clause
                null, // Columns to group by
                null, // Columns to filter by row groups
                null // sort order
        );

        verifyCursor(testDeckValues2, cursor);

        dbHelper.close();
    }

    public void testInsertReadDeckStatistic() {
        MyDbHelper dbHelper = new MyDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // DeckStatistic (Deck 1)
        ContentValues testDeckStatValues1 = new ContentValues();
        testDeckStatValues1.put(MyDbHelper.TITLE, DECK_STATISTIC_1_TITLE);
        testDeckStatValues1.put(MyDbHelper.DECK_SCORE, DECK_STATISTIC_1_SCORE);
        testDeckStatValues1.put(MyDbHelper.DECK_TIME_SPENT, DECK_STATISTIC_1_TIME_SPENT);

        long locationRowId;
        locationRowId = db.insert(MyDbHelper.DECK_STATISTICS_TABLE, null, testDeckStatValues1);

        // Verify row
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        // Query results
        Cursor cursor = db.query(
                MyDbHelper.DECKS_TABLE,
                null, // All columns
                "id = ?", // Columns for where clause
                new String[]{"1"}, // Values for where clause
                null, // Columns to group by
                null, // Columns to filter by row groups
                null // sort order
        );

        assertTrue(cursor.moveToFirst());

        Set<Map.Entry<String, Object>> valueSet = testDeckStatValues1.valueSet();

        for(Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            String expectedValue = String.valueOf(entry.getValue());

            switch (columnName) {
                case "deck_score":
                    assertEquals(expectedValue, "0.93");
                    break;
                case "title":
                    assertEquals(expectedValue, DECK_1_TITLE);
                    break;
                case "deck_time_spent":
                    assertEquals(expectedValue, "34.0");
                    break;
            }
        }

        cursor.close();

        dbHelper.close();
    }

    public void testInsertReadFlashcards() {
        MyDbHelper dbHelper = new MyDbHelper(mContext);
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Flashcard 1 (Deck 1)
        ContentValues testFlashcardValues1 = new ContentValues();
        testFlashcardValues1.put(MyDbHelper.TITLE, FLASHCARD_1_TITLE);
        testFlashcardValues1.put(MyDbHelper.FLASHCARD_QUESTION, FLASHCARD_1_QUESTION);
        testFlashcardValues1.put(MyDbHelper.FLASHCARD_ANSWER, FLASHCARD_1_ANSWER);
        testFlashcardValues1.put(MyDbHelper.BELONGS_TO, 1);

        long locationRowId;
        locationRowId = db.insert(MyDbHelper.FLASHCARDS_TABLE, null, testFlashcardValues1);

        // Verify row
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        // Query results
        Cursor cursor = db.query(
                MyDbHelper.FLASHCARDS_TABLE, // Table to query
                null, // All columns
                "id = ?", // Columns for where clause
                new String[] {"1"}, // Values for where clause
                null, // Columns to group by
                null, // Columns to filter by row groups
                null // Sort order
        );

        verifyCursor(testFlashcardValues1, cursor);

        // Flashcard 2 (Deck 1)
        ContentValues testFlashcardValues2 = new ContentValues();
        testFlashcardValues1.put(MyDbHelper.TITLE, FLASHCARD_2_TITLE);
        testFlashcardValues1.put(MyDbHelper.FLASHCARD_QUESTION, FLASHCARD_2_QUESTION);
        testFlashcardValues1.put(MyDbHelper.FLASHCARD_ANSWER, FLASHCARD_2_ANSWER);
        testFlashcardValues1.put(MyDbHelper.BELONGS_TO, 1);

        locationRowId = db.insert(MyDbHelper.FLASHCARDS_TABLE, null, testFlashcardValues1);

        // Verify row
        assertTrue(locationRowId != -1);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        // Query results
        cursor = db.query(
                MyDbHelper.FLASHCARDS_TABLE, // Table to query
                null, // All columns
                "id = ?", // Columns for where clause
                new String[] {"2"}, // Values for where clause
                null, // Columns to group by
                null, // Columns to filter by row groups
                null // Sort order
        );

        verifyCursor(testFlashcardValues2, cursor);

        // Flashcard 3 (Deck 2)
        ContentValues testFlashcardValues3 = new ContentValues();
        testFlashcardValues2.put(MyDbHelper.TITLE, FLASHCARD_3_TITLE);
        testFlashcardValues2.put(MyDbHelper.FLASHCARD_QUESTION, FLASHCARD_3_QUESTION);
        testFlashcardValues2.put(MyDbHelper.FLASHCARD_ANSWER, FLASHCARD_3_ANSWER);
        testFlashcardValues2.put(MyDbHelper.BELONGS_TO, 2);

        locationRowId = db.insert(MyDbHelper.FLASHCARDS_TABLE, null, testFlashcardValues3);

        // Verify row
        assertTrue(locationRowId != -2);
        Log.d(LOG_TAG, "New row id: " + locationRowId);

        // Query results
        cursor = db.query(
                MyDbHelper.FLASHCARDS_TABLE, // Table to query
                null, // All columns
                "id = ?", // Columns for where clause
                new String[] {"1"}, // Values for where clause
                null, // Columns to group by
                null, // Columns to filter by row groups
                null // Sort order
        );

        verifyCursor(testFlashcardValues3, cursor);

        dbHelper.close();
    }

    private static void verifyCursor(ContentValues expectedValues, Cursor cursorValue) {
        assertTrue(cursorValue.moveToFirst());

        Set<Map.Entry<String, Object>> valueSet = expectedValues.valueSet();

        for(Map.Entry<String, Object> entry : valueSet) {
            String columnName = entry.getKey();
            int index = cursorValue.getColumnIndex(columnName);
            assertFalse(index == -1);

            String expectedValue = entry.getValue().toString();
            assertEquals(expectedValue, cursorValue.getString(index));
        }

        cursorValue.close();
    }
}
