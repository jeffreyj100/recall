package com.jeffreyj100.recall.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import com.jeffreyj100.recall.cardStructures.Subject;
import com.jeffreyj100.recall.cardStructures.Deck;
import com.jeffreyj100.recall.cardStructures.FlashCard;
import com.jeffreyj100.recall.deckStatistics.DeckStatistic;

public class MyDbHelper extends SQLiteOpenHelper {
    private static final String LOG_TAG = "MyDbHelper";

    private static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "flashcards.db";

    // Common Database constants
    public static final String _ID = "id";
    public static final String TITLE = "title";
    public static final String BELONGS_TO = "belongs_to";

    // Subjects Database constants
    public static final String SUBJECTS_TABLE = "subjects";

    // Decks Database constants
    public static final String DECKS_TABLE = "decks";

    // Deck Statistics constants
    public static final String DECK_STATISTICS_TABLE = "deck_statistics";
    public static final String DECK_SCORE = "deck_score";
    public static final String DECK_TIME_SPENT = "deck_time_spent";

    // Flash cards Database table
    public static final String FLASHCARDS_TABLE = "flashcards";
    public static final String FLASHCARD_QUESTION = "question";
    public static final String FLASHCARD_ANSWER = "answer";

    public MyDbHelper(Context context) { super(context, DATABASE_NAME, null, DATABASE_VERSION); }

    @Override
    public void onCreate(SQLiteDatabase db) {
        final String SQL_CREATE_SUBJECT_TABLE = "CREATE TABLE " + SUBJECTS_TABLE + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT," +
                TITLE + " TEXT NOT NULL " +
                ");";

        final String SQL_CREATE_DECKS_TABLE = "CREATE TABLE " + DECKS_TABLE + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TITLE + " TEXT NOT NULL, " +
                BELONGS_TO + " INTEGER NOT NULL, " +
                "FOREIGN KEY (" + BELONGS_TO + ") REFERENCES " + SUBJECTS_TABLE + " (" + _ID + ")" +
                ");";

        final String SQL_CREATE_DECK_STATISTICS_TABLE = "CREATE TABLE " + DECK_STATISTICS_TABLE + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TITLE + " TEXT NOT NULL, " +
                DECK_SCORE + " FLOAT NOT NULL, " +
                DECK_TIME_SPENT + " INTEGER NOT NULL " +
                ");";

        final String SQL_CREATE_FLASHCARDS_TABLE = "CREATE TABLE " + FLASHCARDS_TABLE + " (" +
                _ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                TITLE + " TEXT NOT NULL, " +
                FLASHCARD_QUESTION + " TEXT NOT NULL, " +
                FLASHCARD_ANSWER + " TEXT NOT NULL, " +
                BELONGS_TO + " INTEGER NOT NULL, " +
                "FOREIGN KEY (" + BELONGS_TO + ") REFERENCES " + DECKS_TABLE + " (" + _ID + ")" +
                ");";

        // Create our tables. If table exist, Android ignores these statements
        db.execSQL(SQL_CREATE_SUBJECT_TABLE);
        db.execSQL(SQL_CREATE_DECKS_TABLE);
        db.execSQL(SQL_CREATE_DECK_STATISTICS_TABLE);
        db.execSQL(SQL_CREATE_FLASHCARDS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // TODO change to ALTER TABLE later on...
        db.execSQL("DROP TABLE IF EXISTS " + SUBJECTS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DECKS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + DECK_STATISTICS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + FLASHCARDS_TABLE);
        onCreate(db);
    }

    // Insert new Subject entry in database
    public long createSubject(Subject subject) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITLE, subject.getTitle()); // Insert title of subject

        return db.insert(SUBJECTS_TABLE, null, values);
    }

    // Insert new Deck entry in database
    public long createDeck(Deck deck, long subjectId) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITLE, deck.getTitle());
        values.put(BELONGS_TO, subjectId); // Get id of subjects for reference through foreign key

        return db.insert(DECKS_TABLE, null, values);
    }

    // Insert new Deck Statistic entry in database
    public long createDeckStatistic(DeckStatistic deckStatistic) {
        SQLiteDatabase dbReadable = this.getReadableDatabase();
        SQLiteDatabase dbWritable = this.getWritableDatabase();

        boolean newDeckStat = true;
        long targetDeckStatId = 0;

        List<Float> scores = new ArrayList<>();
        List<Double> times = new ArrayList<>();

        String selectQuery = "SELECT * FROM " + DECK_STATISTICS_TABLE;
        Cursor cursor = dbReadable.rawQuery(selectQuery, null);

        if(cursor.moveToNext()) {
            do {
                String checkDeckName = cursor.getString(cursor.getColumnIndex(TITLE));

                if(checkDeckName.equals(deckStatistic.getDeckName())) { // Existing deck found
                    newDeckStat = false;

                    targetDeckStatId = cursor.getInt(cursor.getColumnIndex(_ID));

                    // Add to temp stats data
                    scores.add(cursor.getFloat(cursor.getColumnIndex(DECK_SCORE)));
                    times.add(cursor.getDouble(cursor.getColumnIndex(DECK_TIME_SPENT)));
                }
            } while(cursor.moveToNext());
        }

        cursor.close();

        if(newDeckStat) { // Insert new deck statistics
            ContentValues values = new ContentValues();
            values.put(TITLE, deckStatistic.getDeckName());
            values.put(DECK_SCORE, deckStatistic.getDeckScore());
            values.put(DECK_TIME_SPENT, deckStatistic.getTimeSpent());

            return dbWritable.insert(DECK_STATISTICS_TABLE, null, values);
        } else if(!newDeckStat) { // Update existing deck stat info
            // Add last deckStatistic argument to temp stats data
            scores.add(deckStatistic.getDeckScore());
            times.add(deckStatistic.getTimeSpent());

            // Calculate new deck stat info
            float scoreTotal = (float) 0.0;
            float scoreAvg;

            double timeSpentTotal = 0.0;
            double timeSpentAvg;

            for(int i = 0; i < scores.size(); i++) {
                scoreTotal += scores.get(i);
                timeSpentTotal += times.get(i);
            }

            scoreAvg = scoreTotal / scores.size();
            scoreAvg = (float) (Math.round(scoreAvg * 100.0) / 100.0); // Round 2 decimal places

            timeSpentAvg = timeSpentTotal / scores.size();
            timeSpentAvg = Math.round(timeSpentAvg * 10.0) / 10.0; // Round 1 decimal place

            return updateDeckStatistic(dbWritable, targetDeckStatId, scoreAvg, timeSpentAvg);
        }

        return 0;
    }

    private long updateDeckStatistic(SQLiteDatabase dbWritable, long rowId, float deckScore, double timeSpent) {
        ContentValues values = new ContentValues();
        values.put(DECK_SCORE, deckScore);
        values.put(DECK_TIME_SPENT, timeSpent);

        return dbWritable.update(DECK_STATISTICS_TABLE, values, _ID + "=" + rowId, null);
    }

    // Insert new Flash Card entry in database
    public long createFlashCard(FlashCard flashCard, long deckId) {
        SQLiteDatabase db = this.getReadableDatabase();
        ContentValues values = new ContentValues();
        values.put(TITLE, "");
        values.put(FLASHCARD_QUESTION, flashCard.getQuestion());
        values.put(FLASHCARD_ANSWER, flashCard.getAnswer());
        values.put(BELONGS_TO, deckId);

        return db.insert(FLASHCARDS_TABLE, null, values);
    }

    public long undoSubject(Subject subject) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(_ID, subject.getId());
        values.put(TITLE, subject.getTitle());

        return db.insert(SUBJECTS_TABLE, null, values);
    }

    public long undoDeck(Deck deck) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(_ID, deck.getId());
        values.put(TITLE, deck.getTitle());
        values.put(BELONGS_TO, deck.getSubject().getId());

        return db.insert(DECKS_TABLE, null, values);
    }

    public long undoFlashCard(FlashCard flashCard) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();
        values.put(_ID, flashCard.getId());
        values.put(TITLE, flashCard.getTitle());
        values.put(FLASHCARD_QUESTION, flashCard.getQuestion());
        values.put(FLASHCARD_ANSWER, flashCard.getAnswer());
        values.put(BELONGS_TO, flashCard.getDeck().getId());

        return db.insert(FLASHCARDS_TABLE, null, values);
    }

    // Get Subject based on it's title
    public Subject getSubjectByTitle(String title) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + SUBJECTS_TABLE + " WHERE " + TITLE + " = " + title + "';";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor != null) {
            cursor.moveToFirst();
        } else {
            Log.i(LOG_TAG, "Cursor is null");
            return null;
        }

        Subject subject = new Subject();
        subject.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
        subject.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
        this.closeDb();
        cursor.close();

        return subject;
    }

    // Get Subject based on it's id
    private Subject getSubjectById(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + SUBJECTS_TABLE + " WHERE " + _ID + " = " + id + ";";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor != null) {
            cursor.moveToFirst();
        } else {
            Log.i(LOG_TAG, "Cursor is null");
            return null;
        }

        Subject subject = new Subject();
        subject.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
        subject.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
        this.closeDb();
        cursor.close();

        return subject;
    }

    // Get Deck based on it's title
    public Deck getDeckByName(String title) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + DECKS_TABLE + " WHERE " + TITLE + " = '" + title + "';";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor != null) {
            cursor.moveToFirst();
        } else {
            Log.i(LOG_TAG, "Cursor is null");
            return null;
        }

        Deck deck = new Deck();
        deck.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
        deck.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));

        Subject subject = getSubjectById(cursor.getLong(cursor.getColumnIndex(BELONGS_TO)));

        deck.setSubject(subject);

        this.closeDb();
        cursor.close();

        return deck;
    }

    // Get Deck based on it's id
    private Deck getDeckById(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + DECKS_TABLE + " WHERE " + _ID + " = " + id + ";";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor != null) {
            cursor.moveToFirst();
        } else {
            Log.i(LOG_TAG, "Cursor is null");
            return null;
        }

        Deck deck = new Deck();
        deck.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
        deck.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));

        Subject subject = getSubjectById(cursor.getLong(cursor.getColumnIndex(BELONGS_TO)));

        if(subject != null) { deck.setSubject(subject); }

        this.closeDb();
        cursor.close();

        return deck;
    }

    // Get Flash Cards based on it's id
    public FlashCard getFlashCardById(long id) {
        SQLiteDatabase db = this.getReadableDatabase();
        String selectQuery = "SELECT * FROM " + FLASHCARDS_TABLE + " WHERE " + _ID + " = " + id + ";";

        Cursor cursor = db.rawQuery(selectQuery, null);
        if(cursor != null) {
            cursor.moveToFirst();
        } else {
            Log.i(LOG_TAG, "Cursor is null");
            return null;
        }

        FlashCard flashCard = new FlashCard();
        flashCard.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
        flashCard.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
        flashCard.setQuestion(cursor.getString(cursor.getColumnIndex(FLASHCARD_QUESTION)));
        flashCard.setAnswer(cursor.getString(cursor.getColumnIndex(FLASHCARD_ANSWER)));

        Deck deck = getDeckById(cursor.getLong(cursor.getColumnIndex(BELONGS_TO)));

        if(deck != null) { flashCard.setDeck(deck); }

        this.closeDb();
        cursor.close();

        return flashCard;
    }

    public List<DeckStatistic> getAllDeckStatistics() {
        List<DeckStatistic> deckStatistics = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + DECK_STATISTICS_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToNext()) {
            do {
                DeckStatistic deckStatistic = new DeckStatistic();
                deckStatistic.setDeckName(cursor.getString(cursor.getColumnIndex(TITLE)));
                deckStatistic.setDeckScore(cursor.getFloat(cursor.getColumnIndex(DECK_SCORE)));
                deckStatistic.setTimeSpent(cursor.getDouble(cursor.getColumnIndex(DECK_TIME_SPENT)));
                deckStatistics.add(deckStatistic);
            } while(cursor.moveToNext());
        }

        cursor.close();

        return deckStatistics;
    }

    public List<FlashCard> getAllFlashCardsForDeckId(Long id) {
        List<FlashCard> flashCards = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + FLASHCARDS_TABLE + " WHERE " + BELONGS_TO + " = " + id + ";";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                FlashCard flashCard = new FlashCard();
                flashCard.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
                flashCard.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                flashCard.setQuestion(cursor.getString(cursor.getColumnIndex(FLASHCARD_QUESTION)));
                flashCard.setAnswer(cursor.getString(cursor.getColumnIndex(FLASHCARD_ANSWER)));
                flashCard.setDeck(getDeckById(id));
                flashCards.add(flashCard);
            } while(cursor.moveToNext());
        }

        cursor.close();
        this.closeDb();

        return flashCards;
    }

    public List<Subject> getAllSubjects() {
        List<Subject> subjects = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + SUBJECTS_TABLE;

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToNext()) {
            do {
                Subject subject = new Subject();
                subject.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
                subject.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                subjects.add(subject);
            } while(cursor.moveToNext());
        }

        cursor.close();

        return subjects;
    }

    public List<Deck> getAllDecksForSubjectId(long id) {
        List<Deck> decks = new ArrayList<>();
        String selectQuery = "SELECT * FROM " + DECKS_TABLE + " WHERE " + BELONGS_TO + " = " + id + ";";

        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                Deck deck = new Deck();
                deck.setId(cursor.getLong(cursor.getColumnIndex(_ID)));
                deck.setTitle(cursor.getString(cursor.getColumnIndex(TITLE)));
                deck.setSubject(getSubjectById(id));
                decks.add(deck);
            } while(cursor.moveToNext());
        }

        cursor.close();
        this.closeDb();

        return decks;
    }

    // General method to delete entry from Database based on it's id
    public void deleteFromDb(long id, String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(table, _ID + " = ?", new String[] { String.valueOf(id) });
        this.closeDb();
    }

    public void deleteAllEntriesForTable(String table) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(table, null, null);
        this.closeDb();
    }

    // Closes Database connection
    private void closeDb() {
        SQLiteDatabase db = this.getReadableDatabase();
        if(db != null && db.isOpen()) { db.close(); }
    }
}
