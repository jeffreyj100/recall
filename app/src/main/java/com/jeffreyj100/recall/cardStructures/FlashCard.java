package com.jeffreyj100.recall.cardStructures;

public class FlashCard extends CardItem {
    private Subject subject;
    private Deck deck;
    private String question, answer;

    // Basic constructor
    public FlashCard() { }

    public FlashCard(Subject subject, Deck deck, String title, String question, String answer) {
        super(title);
        this.subject = subject;
        this.deck = deck;
        this.question = question;
        this.answer = answer;
    }

    // Getters and Setters
    public Subject getSubject() { return subject; }
    public void setSubject(Subject subject) { this.subject = subject; }

    public Deck getDeck() { return deck; }
    public void setDeck(Deck deck) { this.deck = deck; }

    public String getQuestion() { return question; }
    public void setQuestion(String question) { this.question = question; }

    public String getAnswer() { return answer; }
    public void setAnswer(String answer) { this.answer = answer; }
}
