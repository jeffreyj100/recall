package com.jeffreyj100.recall.cardStructures;

import java.util.ArrayList;

public class Subject extends CardItem {
    private ArrayList<Deck> decks; // List of decks that belong to this subject

    // Basic constructor
    public Subject() { }

    public Subject(String subjectName, ArrayList<Deck> decks) {
        super(subjectName);
        this.decks = decks;
    }

    // Getters and Setters
    public ArrayList<Deck> getDecks() { return decks; }
    public void setDecks(ArrayList<Deck> decks) { this.decks = decks; }
}
