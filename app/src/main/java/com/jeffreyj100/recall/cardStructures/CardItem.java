package com.jeffreyj100.recall.cardStructures;

public abstract class CardItem {
    private String title; // All cards must have title to display on list
    private long id; // Each card must have an id; used to find it in database

    // Basic constructor
    CardItem() { }

    CardItem(String title) { this.title = title; }

    // Getters and Setters
    public String getTitle() { return title; }
    public void setTitle(String title) { this.title = title; }

    public long getId() { return id; }
    public void setId(long id) { this.id = id; }
}
