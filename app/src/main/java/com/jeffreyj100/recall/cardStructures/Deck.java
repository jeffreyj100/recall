package com.jeffreyj100.recall.cardStructures;

import java.util.ArrayList;

public class Deck extends CardItem {
    private Subject subject;
    private ArrayList<FlashCard> flashCards;

    // Basic constructor
    public Deck() { }

    public Deck(Subject subject, String deckName, ArrayList<FlashCard> flashCards) {
        super(deckName);
        this.subject = subject;
        this.flashCards = flashCards;
    }

    // Getters and Setters
    public Subject getSubject() { return subject; }
    public void setSubject(Subject subject) { this.subject = subject; }

    public ArrayList<FlashCard> getFlashCards() { return flashCards; }
    public void setFlashCards(ArrayList<FlashCard> flashCards) { this.flashCards = flashCards; }
}
