package com.jeffreyj100.recall.cards;

import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.SettingsPreferences;
import com.jeffreyj100.recall.cardStructures.FlashCard;
import com.jeffreyj100.recall.database.MyDbHelper;
import com.jeffreyj100.recall.deckStatistics.DeckStatistic;
import com.jeffreyj100.recall.fragments.FragmentFlashcardViewer;
import com.jeffreyj100.recall.fragments.FragmentFlashcardsList;

import it.gmariotti.cardslib.library.internal.Card;

public class FlashcardViewerCard extends Card {
    // Instance variables used by this fragment
    private FlashCard card;
    private TextView questionTextView;
    private TextView answerTextView;
    private Button showAnswerButton;
    private Button correctButton;
    private Button wrongButton;
    private LinearLayout layout;

    public FlashcardViewerCard(Context context) {
        super(context, R.layout.flashcard_content);
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        layout = (LinearLayout) view.findViewById(R.id.linearLayoutFlashcardContent);

        questionTextView = (TextView) view.findViewById(R.id.questionTextView);
        answerTextView = (TextView) view.findViewById(R.id.answerTextView);

        // Get users settings for card text size
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        String textSizeSettingString = sharedPreferences.getString(SettingsPreferences.KEY_CARD_SIZE, null);

        float textSizeSetting = (float) 16.0;

        if(textSizeSettingString != null) {
            textSizeSettingString = sharedPreferences.getString(SettingsPreferences.KEY_CARD_SIZE, "");
            textSizeSetting = Float.parseFloat(textSizeSettingString);
        }

        questionTextView.setTextSize(textSizeSetting);
        answerTextView.setTextSize(textSizeSetting);

        if (card != null) {
            questionTextView.setText(card.getQuestion());
            answerTextView.setText(card.getAnswer());
        }

        buttonsHandler(view);
    }

    private void buttonsHandler(View view) {
        showAnswerButton = (Button) view.findViewById(R.id.showAnswerButton);
        showAnswerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                answerTextView.setVisibility(View.VISIBLE);
                showAnswerButton.setVisibility(View.GONE);

                correctButton = new Button(getContext());
                correctButton.setTextColor(Color.parseColor("#FFFFFF"));
                correctButton.setBackgroundColor(Color.parseColor("#4CAF50"));

                wrongButton = new Button(getContext());
                wrongButton.setTextColor(Color.parseColor("#FFFFFF"));
                wrongButton.setBackgroundColor(Color.parseColor("#D32F2F"));

                // Allow lower and uppercase letters
                correctButton.setTransformationMethod(null);
                wrongButton.setTransformationMethod(null);

                correctButton.setText("Correct");
                wrongButton.setText("Wrong");

                correctButton.setTextSize(16);
                wrongButton.setTextSize(16);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
                params.setMargins(0, 22, 0, 0);
                correctButton.setLayoutParams(params);
                wrongButton.setLayoutParams(params);

                layout.addView(correctButton);
                layout.addView(wrongButton);

                clickedAnswerOptions(correctButton);
                clickedAnswerOptions(wrongButton);
            }
        });
    }

    private void clickedAnswerOptions(final Button button) {
        View.OnClickListener buttonClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (button == correctButton) {
                    FragmentFlashcardViewer.numRight += 1;
                } else if (button == wrongButton) {
                    FragmentFlashcardViewer.numWrong += 1;
                    FragmentFlashcardViewer.numMissedCards += 1;
                }

                FragmentFlashcardViewer.numRightTextView.setText("Right:  " + FragmentFlashcardViewer.numRight);
                FragmentFlashcardViewer.numWrongTextView.setText("Wrong:  " + FragmentFlashcardViewer.numWrong);
                checkIfLast();
            }
        };

        button.setOnClickListener(buttonClickListener);
    }

    private void checkIfLast() {
        // If last card reached
        if ((FragmentFlashcardViewer.mPager.getCurrentItem() + 1) == FragmentFlashcardViewer.numQuestions) {
            MyDbHelper dbHelper = new MyDbHelper(getContext());
            DeckStatistic deckStatistic = new DeckStatistic();

            deckStatistic.setDeckName(FragmentFlashcardViewer.activeBundle.getString(MyDbHelper.TITLE));

            // Record end time and total study time length
            FragmentFlashcardViewer.tEnd = System.currentTimeMillis();
            FragmentFlashcardViewer.tDelta = FragmentFlashcardViewer.tEnd - FragmentFlashcardViewer.tStart;
            FragmentFlashcardViewer.timeSpent = FragmentFlashcardViewer.tDelta / 1000.00;
            deckStatistic.setTimeSpent(FragmentFlashcardViewer.timeSpent);

            // Calculate study session score
            FragmentFlashcardViewer.sessionScore = FragmentFlashcardViewer.numRight / (float) FragmentFlashcardViewer.numQuestions;
            deckStatistic.setDeckScore(FragmentFlashcardViewer.sessionScore);

            // Add new deck statistic to db
            try {
                dbHelper.createDeckStatistic(deckStatistic);
            } catch (Exception e) {
                Log.i("logging", "Could not create deck statistic = " + e);
            }

            // Get users repeatStudy setting
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            boolean repeatStudy = sharedPreferences.getBoolean(SettingsPreferences.KEY_REPEAT_STUDY, true);

            // Review missed cards again
            if ((FragmentFlashcardViewer.numMissedCards > 0) && repeatStudy) {
                MainActivity.fm.popBackStack(); // Clean back stack
                ((MainActivity) getContext()).displayView(MainActivity.FLASHCARDS_VIEWER, FragmentFlashcardsList.bundle);
                FragmentFlashcardViewer.screenSlidePagerAdapter.notifyDataSetChanged(); // Match adapter count
            // Finish studying
            } else {
                questionTextView.setVisibility(View.GONE);
                correctButton.setVisibility(View.GONE);
                wrongButton.setVisibility(View.GONE);

                answerTextView.setText("Studying is complete.");

                Button backToDeckButton = new Button(getContext());
                backToDeckButton.setTransformationMethod(null);
                backToDeckButton.setText("Back to Deck");
                backToDeckButton.setTextColor(Color.parseColor("#FFFFFF"));
                backToDeckButton.setBackgroundColor(Color.parseColor("#607D8B"));
                layout.addView(backToDeckButton);

                backToDeckButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        MainActivity.fm.popBackStack(); // Return to deck list
                    }
                });
            }
        // Go to next card
        } else {
            FragmentFlashcardViewer.mPager.setCurrentItem(FragmentFlashcardViewer.getItem(+1), true);
        }
    }

    public FlashCard getCard() {
        return card;
    }

    public void setCard(FlashCard card) {
        this.card = card;
    }
}
