package com.jeffreyj100.recall.cards;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.cardStructures.FlashCard;
import com.jeffreyj100.recall.database.MyDbHelper;
import com.jeffreyj100.recall.fragments.FragmentFlashcardsList;

import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;

public class ListFlashcardCard extends Card implements
        Card.OnCardClickListener,
        Card.OnSwipeListener,
        Card.OnUndoSwipeListListener {
    // Instance variables
    private FlashCard flashCard;
    private MyDbHelper dbHelper;

    public ListFlashcardCard(Context context) {
        super(context, R.layout.card_flashcard);
        dbHelper = new MyDbHelper(context);
        this.setSwipeable(true);
        this.setOnClickListener(this);
        this.setOnSwipeListener(this);
        this.setOnUndoSwipeListListener(this);
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        TextView flashCardTitle = (TextView) view.findViewById(R.id.titleFlashcard);

        if(flashCard != null) {
            String title = flashCard.getQuestion().length() > 40 ? flashCard.getQuestion().substring(0, 40) + "..." : flashCard.getQuestion();
            flashCardTitle.setText(title);
        }
    }

    @Override
    public void onClick(Card card, View view) { }

    @Override
    public void onSwipe(Card card) {
        dbHelper.deleteFromDb(flashCard.getId(), MyDbHelper.FLASHCARDS_TABLE);
        Toast.makeText(getContext(), "Flash card removed from deck", Toast.LENGTH_SHORT).show();

        // Hide study button if no cards remain
        List<FlashCard> flashCards = dbHelper.getAllFlashCardsForDeckId(flashCard.getDeck().getId());
        if(flashCards.size() < 1) {
            FragmentFlashcardsList.studyButton.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    public void onUndoSwipe(Card card) { dbHelper.undoFlashCard(flashCard); }

    public FlashCard getFlashCard() { return flashCard; }

    public void setFlashCard(FlashCard flashCard) { this.flashCard = flashCard; }
}
