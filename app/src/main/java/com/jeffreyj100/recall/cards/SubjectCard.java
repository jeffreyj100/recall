package com.jeffreyj100.recall.cards;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.cardStructures.Subject;
import com.jeffreyj100.recall.database.MyDbHelper;

import it.gmariotti.cardslib.library.internal.Card;

public class SubjectCard extends Card implements
        Card.OnSwipeListener,
        Card.OnCardClickListener,
        Card.OnUndoHideSwipeListListener {
    private Subject subject;
    private MyDbHelper dbHelper;

    public SubjectCard(Context context) {
        super(context, R.layout.card_subject);
        dbHelper = new MyDbHelper(context);
        this.setSwipeable(true);
        this.setOnClickListener(this);
        this.setOnSwipeListener(this);
        this.setOnUndoHideSwipeListListener(this);
    }

    public Subject getSubject() { return subject; }

    public void setSubject(Subject subject) { this.subject = subject; }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        TextView subjectTitle = (TextView) view.findViewById(R.id.titleSubject);

        if(subject != null) { subjectTitle.setText(subject.getTitle()); }
    }

    @Override
    public void onClick(Card card, View view) {
        Bundle bundle = new Bundle();
        bundle.putLong(MyDbHelper._ID, subject.getId());
        bundle.putString(MyDbHelper.TITLE, subject.getTitle());

        // Switch to DecksFragment
        ((MainActivity) getContext()).displayView(MainActivity.DECKS_FRAG, bundle);
    }

    @Override
    public void onSwipe(Card card) {
        dbHelper.deleteFromDb(subject.getId(), MyDbHelper.SUBJECTS_TABLE);
        Toast.makeText(getContext(), "Subject deleted", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUndoHideSwipe(Card card) { dbHelper.undoSubject(subject); }
}
