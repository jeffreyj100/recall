package com.jeffreyj100.recall.cards;

import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.cardStructures.Deck;
import com.jeffreyj100.recall.database.MyDbHelper;

import it.gmariotti.cardslib.library.internal.Card;

public class DeckCard extends Card implements
        Card.OnCardClickListener,
        Card.OnSwipeListener,
        Card.OnUndoSwipeListListener {
    private Deck deck;
    private MyDbHelper dbHelper;

    public DeckCard(Context context) {
        super(context, R.layout.card_deck);
        dbHelper = new MyDbHelper(context);
        this.setSwipeable(true);
        this.setOnClickListener(this);
        this.setOnSwipeListener(this);
        this.setOnUndoSwipeListListener(this);
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {
        super.setupInnerViewElements(parent, view);

        TextView deckTitle = (TextView) view.findViewById(R.id.titleDeck);

        if (deck != null) {
            deckTitle.setText(deck.getTitle());
        }
    }

    @Override
    public void onClick(Card card, View view) {
        Bundle bundle = new Bundle();
        bundle.putLong(MyDbHelper._ID, deck.getId());
        bundle.putString(MyDbHelper.TITLE, deck.getTitle());

        // Switch to FragmentDecks
        ((MainActivity) getContext()).displayView(MainActivity.FLASHCARDS_FRAG, bundle);
    }

    @Override
    public void onSwipe(Card card) {
        dbHelper.deleteFromDb(deck.getId(), MyDbHelper.DECKS_TABLE);
        Toast.makeText(getContext(), "Deck removed from subject", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onUndoSwipe(Card card) {
        dbHelper.undoDeck(deck);
    }

    public Deck getDeck() {
        return deck;
    }

    public void setDeck(Deck deck) {
        this.deck = deck;
    }
}
