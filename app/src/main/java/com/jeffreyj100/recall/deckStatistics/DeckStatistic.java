package com.jeffreyj100.recall.deckStatistics;

import com.jeffreyj100.recall.cardStructures.Subject;

public class DeckStatistic {
    private Subject subject;
    private String deckName;
    private float deckScore;
    private double timeSpent; // In seconds

    // Basic constructor
    public DeckStatistic() { }

    public DeckStatistic(Subject subject, String deckName, float deckScore, int timeSpent) {
        this.subject = subject;
        this.deckName = deckName;
        this.deckScore = deckScore;
        this.timeSpent = timeSpent;
    }

    // Getters and Setters
    public Subject getSubject() { return subject; }
    public void setSubject(Subject subject) { this.subject = subject; }

    public String getDeckName() { return deckName; }
    public void setDeckName(String deckName) { this.deckName = deckName; }

    public float getDeckScore() { return deckScore; }
    public void setDeckScore(float deckScore) { this.deckScore = deckScore; }

    public double getTimeSpent() { return timeSpent; }
    public void setTimeSpent(double timeSpent) { this.timeSpent = timeSpent; }
}
