package com.jeffreyj100.recall;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;
import android.view.KeyEvent;

@SuppressWarnings("deprecation")
public class SettingsPreferences extends PreferenceActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {
    public static String NIGHT_MODE = "toggleNightDayTheme";
    public static String KEY_CARD_SIZE = "cardSize";
    public static String KEY_REPEAT_STUDY = "repeatStudyUntilCorrect";
    public static String KEY_RANDOM_ORDER = "randomCardOrder";

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.user_settings);

        // Clear shared preferences
        //sharedPreferences.edit().clear().commit();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            Intent intent = new Intent(this, MainActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();
        }

        return super.onKeyDown(keyCode, event);
    }
}
