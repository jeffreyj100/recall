package com.jeffreyj100.recall;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.IntentCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.app.AppCompatDelegate;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;

import com.jeffreyj100.recall.fragments.FragmentStatistics;
import com.jeffreyj100.recall.fragments.FragmentSubjects;

public abstract class NavigationDrawerActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {
    private Class currentFragmentClass;
    private FragmentManager fm;
    private NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        if (savedInstanceState == null) {
            switchFragments(FragmentSubjects.class); // Default fragment on start

            // Toggle night mode from settings
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getApplication());
            boolean nightMode = sharedPreferences.getBoolean(SettingsPreferences.NIGHT_MODE, false);

            if(nightMode) {
                getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                recreate();
            } else if(!nightMode) {
                getDelegate().setLocalNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                recreate();
            }
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        // Set subjects as selected view on start
        if (savedInstanceState == null) {
            Menu menu = navigationView.getMenu();
            MenuItem menuItem = menu.getItem(0);
            menuItem.setChecked(true);
        }
    }

    @Override
    public void onBackPressed() {
        // Return to main content when nav drawer open on back press
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        currentFragmentClass = null;

        if (id == R.id.nav_subjects) {
            currentFragmentClass = FragmentSubjects.class;
            switchFragments(currentFragmentClass);
        } else if (id == R.id.nav_statistics) {
            currentFragmentClass = FragmentStatistics.class;
            switchFragments(currentFragmentClass);
        } else if (id == R.id.nav_settings) {
            Intent intent = new Intent(this, SettingsPreferences.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);

        return true;
    }

    // Define what clicking item will do
    public abstract void displayView(int position, Bundle fragmentBundle);

    private void switchFragments(Class fragmentClassPicked) {
        currentFragmentClass = fragmentClassPicked;
        Fragment newFragment = null;

        try {
            newFragment = (Fragment) fragmentClassPicked.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();
        ft.replace(R.id.frame_content, newFragment);
        ft.commit();
    }
}
