package com.jeffreyj100.recall.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.SettingsPreferences;
import com.jeffreyj100.recall.cardStructures.FlashCard;
import com.jeffreyj100.recall.cards.ListFlashcardCard;
import com.jeffreyj100.recall.database.MyDbHelper;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

public class FragmentFlashcardsList extends Fragment {
    public static Button studyButton;
    public static Bundle bundle;

    // Constructors
    public FragmentFlashcardsList() { }

    public static FragmentFlashcardsList newInstance() {
        return new FragmentFlashcardsList();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this Fragment
        return inflater.inflate(R.layout.fragment_flashcards, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FloatingActionButton addNewFlashcardFab = (FloatingActionButton) view.findViewById(R.id.addNewFlashcard);
        addNewFlashcardFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Switch to FragmentNewFlashcard
                ((MainActivity) getActivity()).displayView(MainActivity.NEW_FLASHCARD_FRAG, bundle);
            }
        });

        // Initialize CardsList
        CardListView mFlashcardsList = (CardListView) view.findViewById(R.id.flashCardList);
        List<Card> mCardsList = new ArrayList<>();

        bundle = getArguments();
        getActivity().setTitle(bundle.getString(MyDbHelper.TITLE));

        // Get all flash cards under deck
        MyDbHelper dbHelper = new MyDbHelper(getActivity());
        List<FlashCard> flashCards = dbHelper.getAllFlashCardsForDeckId(bundle.getLong(MyDbHelper._ID));

        for(int i = 0; i < flashCards.size(); i++) {
            ListFlashcardCard card = new ListFlashcardCard(getActivity());

            // Toggle night/day theme
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            boolean nightMode = sharedPreferences.getBoolean(SettingsPreferences.NIGHT_MODE, false);
            if(nightMode) {
                card.setBackgroundResourceId(R.color.colorPrimaryDark);
            }
            else if(!nightMode) {
                card.setBackgroundResourceId(R.drawable.card_background);
            }

            card.setId(String.valueOf(flashCards.get(i).getId()));
            card.setFlashCard(flashCards.get(i));
            mCardsList.add(card);
        }

        CardArrayAdapter mCardAdapter = new CardArrayAdapter(getActivity(), mCardsList);
        mCardAdapter.setEnableUndo(true);
        mFlashcardsList.setAdapter(mCardAdapter);

        studyButton = (Button) view.findViewById(R.id.study);
        studyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Switch to FragmentStudyCards
                ((MainActivity) getActivity()).displayView(MainActivity.FLASHCARDS_VIEWER, bundle);
            }
        });

        if(flashCards.size() > 0) { // Only display study button if cards exist
            studyButton.setVisibility(View.VISIBLE);
        }
    }
}
