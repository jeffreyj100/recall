package com.jeffreyj100.recall.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.cards.FlashcardViewerCard;

import it.gmariotti.cardslib.library.view.CardView;

public class FragmentFlashcard extends Fragment {
    private FlashcardViewerCard card;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_flashcard_content_viewer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        LinearLayout layout = (LinearLayout) view.findViewById(R.id.flashcardRelativeLayout);
        CardView cardView = new CardView(getActivity());
        cardView.setCard(card);
        layout.addView(cardView);
    }

    public FlashcardViewerCard getCard() { return card; }
    public void setCard(FlashcardViewerCard card) {
        this.card = card;
    }
}
