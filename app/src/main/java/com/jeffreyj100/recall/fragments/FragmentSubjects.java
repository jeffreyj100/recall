package com.jeffreyj100.recall.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.SettingsPreferences;
import com.jeffreyj100.recall.cardStructures.Subject;
import com.jeffreyj100.recall.cards.SubjectCard;
import com.jeffreyj100.recall.database.MyDbHelper;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

public class FragmentSubjects extends Fragment {
    private FloatingActionButton addNewSubjectFab;

    // Constructors
    public FragmentSubjects() { }

    public static FragmentSubjects newInstance() {
        return new FragmentSubjects();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this Fragment
        return inflater.inflate(R.layout.fragment_subjects, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("Subjects");

        addNewSubjectFab = (FloatingActionButton) view.findViewById(R.id.addNewSubject);
        addNewSubjectFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Switch to FragmentNewSubject Fragment
                if(v.getId() == addNewSubjectFab.getId()) {
                    ((MainActivity) getActivity()).displayView(MainActivity.NEW_SUBJECT_FRAG, null);
                }
            }
        });

        // Initialize CardList
        CardListView mSubjectsList = (CardListView) view.findViewById(R.id.subjectsList);
        List<Card> mCardsList = new ArrayList<>();

        // Get all subjects from db
        MyDbHelper dbHelper = new MyDbHelper(getActivity());
        List<Subject> subjects = dbHelper.getAllSubjects();

        for(int i = 0; i < subjects.size(); i++) {
            SubjectCard card = new SubjectCard(getActivity());

            // Toggle night/day theme
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            boolean nightMode = sharedPreferences.getBoolean(SettingsPreferences.NIGHT_MODE, false);
            if(nightMode) {
                card.setBackgroundResourceId(R.color.colorPrimaryDark);
            }
            else if(!nightMode) {
                card.setBackgroundResourceId(R.drawable.card_background);
            }

            card.setId(String.valueOf(subjects.get(i).getId()));
            card.setSubject(subjects.get(i));
            mCardsList.add(card);
        }

        CardArrayAdapter mCardAdapter = new CardArrayAdapter(getActivity(), mCardsList);
        mCardAdapter.setEnableUndo(true);
        mSubjectsList.setAdapter(mCardAdapter);
    }
}
