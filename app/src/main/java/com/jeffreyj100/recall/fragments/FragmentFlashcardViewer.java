package com.jeffreyj100.recall.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.SettingsPreferences;
import com.jeffreyj100.recall.NonSwipeableViewPager;
import com.jeffreyj100.recall.cardStructures.FlashCard;
import com.jeffreyj100.recall.cards.FlashcardViewerCard;
import com.jeffreyj100.recall.database.MyDbHelper;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class FragmentFlashcardViewer extends Fragment {
    // Instance variables used by this Fragment
    public static int numMissedCards;
    private static List<FlashcardViewerCard> mCardsList;
    public static int numQuestions;

    public static Bundle activeBundle;

    public static NonSwipeableViewPager mPager; // Pager widget
    public static ScreenSlidePagerAdapter screenSlidePagerAdapter; // Pager adapter

    public static int numRight;
    public static int numWrong;
    public static float sessionScore;

    public static long tStart;
    public static long tEnd;
    public static long tDelta;
    public static double timeSpent;

    public static TextView numRightTextView;
    public static TextView numWrongTextView;
    private static TextView numTotalCardsTextView;

    // Constructors
    public FragmentFlashcardViewer() {
    }

    public static FragmentFlashcardViewer newInstance() {
        FragmentFlashcardViewer fragmentFlashcardViewer = new FragmentFlashcardViewer();
        numRight = 0;
        numWrong = 0;
        numQuestions = 0;
        numMissedCards = 0;

        tStart = (long) 0.0;
        tEnd = (long) 0.0;
        tDelta = (long) 0.0;
        timeSpent = 0.0;

        return fragmentFlashcardViewer;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this Fragment
        return inflater.inflate(R.layout.fragment_flashcard_viewer, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        activeBundle = getArguments();

        if (activeBundle == null) {
            activeBundle = savedInstanceState;
        }

        // Instantiate number totals text views
        numRightTextView = (TextView) view.findViewById(R.id.numRightTextView);
        numWrongTextView = (TextView) view.findViewById(R.id.numWrongTextView);
        numTotalCardsTextView = (TextView) view.findViewById(R.id.numTotalCardsTextView);

        // Grab all flashcards of deck from database
        MyDbHelper dbHelper = new MyDbHelper(getActivity());
        mCardsList = new ArrayList<>();
        List<FlashCard> flashcards = dbHelper.getAllFlashCardsForDeckId(activeBundle.getLong(MyDbHelper._ID));

        // Get user's random order setting
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        boolean shuffleCards = sharedPreferences.getBoolean(SettingsPreferences.KEY_RANDOM_ORDER, true);

        // Mix up order of flashcards
        if(shuffleCards) {
            long seed = System.nanoTime();
            Collections.shuffle(flashcards, new Random(seed));
        }

        numQuestions = flashcards.size();

        for (int i = 0; i < flashcards.size(); i++) {
            FlashcardViewerCard card = new FlashcardViewerCard(getActivity());

            // Toggle night/day theme
            boolean nightMode = sharedPreferences.getBoolean(SettingsPreferences.NIGHT_MODE, false);
            if(nightMode) {
                card.setBackgroundResourceId(R.color.colorPrimaryDark);
            }
            else if(!nightMode) {
                card.setBackgroundResourceId(R.drawable.card_background);
            }

            card.setId(String.valueOf(flashcards.get(i).getId()));
            card.setCard(flashcards.get(i));
            mCardsList.add(card);
        }

        numTotalCardsTextView.setText("Total:  " + numQuestions);

        // Instantiate ViewPager & PagerAdapter
        screenSlidePagerAdapter = new ScreenSlidePagerAdapter(getFragmentManager());
        mPager = (NonSwipeableViewPager) view.findViewById(R.id.pager);
        mPager.setAdapter(screenSlidePagerAdapter);
        mPager.setCurrentItem(0);

        // Start timer
        tStart = System.currentTimeMillis();
    }

    public static int getItem(int i) {
        return mPager.getCurrentItem() + i;
    }

    public static class ScreenSlidePagerAdapter extends FragmentStatePagerAdapter {
        private Fragment mFragment;

        public ScreenSlidePagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            mFragment = new FragmentFlashcard();
            ((FragmentFlashcard) mFragment).setCard(mCardsList.get(position));

            return mFragment;
        }

        @Override
        public int getCount() {
            return numQuestions;
        }

        @Override
        public int getItemPosition(Object object) { return super.getItemPosition(object); }

        @Override
        public void notifyDataSetChanged() {
            super.notifyDataSetChanged();
        }
    }
}
