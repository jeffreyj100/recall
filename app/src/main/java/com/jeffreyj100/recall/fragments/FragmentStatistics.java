package com.jeffreyj100.recall.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.SettingsPreferences;
import com.jeffreyj100.recall.fragments.statisticsTab.FragmentLeastAccurateTab;
import com.jeffreyj100.recall.fragments.statisticsTab.FragmentMostAccurateTab;
import com.jeffreyj100.recall.fragments.statisticsTab.FragmentTimeSpentTab;
import com.jeffreyj100.recall.tabs.SlidingTabLayout;

public class FragmentStatistics extends Fragment {
    // Instance variables
    private ViewPager viewPager;
    private ViewPagerAdapter viewPagerAdapter;
    private SlidingTabLayout tabs;
    private CharSequence[] titles = {"Least Accurate", "Most Accurate", "Time Spent Avg"};
    private int NUM_TABS = 3;
    public static int MAX_ITEMS = 10;

    // Constructors
    public FragmentStatistics() { }

    public static FragmentStatistics newInstance() {
        return new FragmentStatistics();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate layout for this fragment
        return inflater.inflate(R.layout.fragment_statistics, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        getActivity().setTitle("Statistics");

        viewPagerAdapter = new ViewPagerAdapter(getFragmentManager(), titles, NUM_TABS);
        viewPager = (ViewPager) view.findViewById(R.id.statisticsPager);
        viewPager.setAdapter(viewPagerAdapter);

        tabs = (SlidingTabLayout) view.findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true);

        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return ContextCompat.getColor(getContext(), R.color.tabScrollColor);
            }
        });

        tabs.setViewPager(viewPager);
    }

    public static class ViewPagerAdapter extends FragmentStatePagerAdapter {
        CharSequence titles[];
        int numTabs;

        public ViewPagerAdapter(FragmentManager fm, CharSequence mTitles[], int mNumTabs) {
            super(fm);
            this.titles = mTitles;
            this.numTabs = mNumTabs;
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0) {
                return new FragmentLeastAccurateTab();
            } else if(position == 1) {
                return new FragmentMostAccurateTab();
            } else {
                return new FragmentTimeSpentTab();
            }
        }

        @Override
        public CharSequence getPageTitle(int position) { return titles[position]; }

        @Override
        public int getCount() {
            return numTabs;
        }
    }
}
