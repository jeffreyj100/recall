package com.jeffreyj100.recall.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.Utils;
import com.jeffreyj100.recall.cardStructures.FlashCard;
import com.jeffreyj100.recall.database.MyDbHelper;

public class FragmentNewFlashcard extends Fragment {
    // Instance variables used by this Fragment
    private Button addNewFlashcardButton;
    private EditText flashCardQuestionEditText, flashCardAnswerEditText;
    private Bundle bundle;

    // Constructors
    public FragmentNewFlashcard() { }

    public static FragmentNewFlashcard newInstance() {
        return new FragmentNewFlashcard();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_flashcard, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        bundle = getArguments();

        addNewFlashcardButton = (Button) view.findViewById(R.id.createFlashcard);
        addNewFlashcardButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == addNewFlashcardButton.getId()) {
                    MyDbHelper dbHelper = new MyDbHelper(getActivity());
                    FlashCard flashCard = new FlashCard();

                    if(!flashCardQuestionEditText.getText().toString().isEmpty() && !flashCardAnswerEditText.getText().toString().isEmpty()) {
                        flashCard.setQuestion(flashCardQuestionEditText.getText().toString());
                        flashCard.setAnswer(flashCardAnswerEditText.getText().toString());

                        try {
                            dbHelper.createFlashCard(flashCard, bundle.getLong(MyDbHelper._ID));
                            Utils.hideKeyboard(getActivity());

                            // Switch to FragmentFlashcardsList
                            clearBackStack();
                            ((MainActivity) getActivity()).displayView(MainActivity.FLASHCARDS_FRAG, bundle);
                        } catch(Exception e) {
                            Log.i("logging", "Could not create Flashcard = " + e);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Flash card must have a question and answer", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });

        flashCardQuestionEditText = (EditText) view.findViewById(R.id.flashcard_question);
        flashCardQuestionEditText.setHorizontallyScrolling(false);
        flashCardQuestionEditText.setMaxLines(5);

        flashCardAnswerEditText = (EditText) view.findViewById(R.id.flashcard_answer);
        flashCardAnswerEditText.setHorizontallyScrolling(false);
        flashCardAnswerEditText.setMaxLines(5);
    }

    // Clear back stack to avoid back stack bugs
    private void clearBackStack() {
        FragmentManager manager = getFragmentManager();

        for(int i = 0; i < manager.getBackStackEntryCount(); i++) {
            manager.popBackStack();
        }
    }
}
