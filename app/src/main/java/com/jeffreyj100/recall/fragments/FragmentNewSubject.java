package com.jeffreyj100.recall.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.Utils;
import com.jeffreyj100.recall.cardStructures.Subject;
import com.jeffreyj100.recall.database.MyDbHelper;

public class FragmentNewSubject extends Fragment {
    // Instance variables used by this Fragment
    private Button addSubjectButton;
    private EditText subjectNameEditText;

    // Constructors
    public FragmentNewSubject() { }

    public static FragmentNewSubject newInstance() {
        return new FragmentNewSubject();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_new_subject, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        addSubjectButton = (Button) view.findViewById(R.id.submitNewSubject);
        subjectNameEditText = (EditText) view.findViewById(R.id.subjectTitle);
        addSubjectButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(v.getId() == addSubjectButton.getId()) {
                    MyDbHelper dbHelper = new MyDbHelper(getActivity());
                    Subject subject = new Subject();

                    if(!subjectNameEditText.getText().toString().isEmpty()) {
                        subject.setTitle(subjectNameEditText.getText().toString());

                        try {
                            dbHelper.createSubject(subject);
                            Utils.hideKeyboard(getActivity());

                            // Switch to FragmentSubjects
                            clearBackStack();
                            ((MainActivity) getActivity()).displayView(MainActivity.SUBJECTS_FRAG, null);
                        } catch(Exception e) {
                            Log.i("logging", "Could not create subject = " + e);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Subject must have a title", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    // Clear back stack to avoid back stack bugs
    private void clearBackStack() {
        FragmentManager manager = getFragmentManager();

        for(int i = 0; i < manager.getBackStackEntryCount(); i++) {
            manager.popBackStack();
        }
    }
}
