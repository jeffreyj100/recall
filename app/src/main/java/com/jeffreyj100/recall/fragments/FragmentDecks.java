package com.jeffreyj100.recall.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.SettingsPreferences;
import com.jeffreyj100.recall.cardStructures.Deck;
import com.jeffreyj100.recall.cards.DeckCard;
import com.jeffreyj100.recall.database.MyDbHelper;

import java.util.ArrayList;
import java.util.List;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.view.CardListView;

public class FragmentDecks extends Fragment {
    private String title;
    private long id;

    // Constructors
    public FragmentDecks() { }

    public static FragmentDecks newInstance() {
        return new FragmentDecks();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this Fragment
        return inflater.inflate(R.layout.fragment_decks, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        FloatingActionButton addNewDeckFab = (FloatingActionButton) view.findViewById(R.id.addNewDeck);
        addNewDeckFab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Bundle bundle = new Bundle();
                bundle.putLong(MyDbHelper._ID, id);
                bundle.putString(MyDbHelper.TITLE, title);

                // Switch to FragmentNewDeck Fragment
                ((MainActivity) getActivity()).displayView(MainActivity.NEW_DECK_FRAG, bundle);
            }
        });

        // Initialize CardsList
        CardListView mDecksList = (CardListView) view.findViewById(R.id.deckList);
        List<Card> mCardsList = new ArrayList<>();

        Bundle bundle = getArguments();
        title = bundle.getString(MyDbHelper.TITLE);
        id = bundle.getLong(MyDbHelper._ID);
        getActivity().setTitle(title);

        // Get all decks under subject
        MyDbHelper dbHelper = new MyDbHelper(getActivity());
        List<Deck> decks = dbHelper.getAllDecksForSubjectId(id);

        for(int i = 0; i < decks.size(); i++) {
            DeckCard card = new DeckCard(getActivity());

            // Toggle night/day theme
            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
            boolean nightMode = sharedPreferences.getBoolean(SettingsPreferences.NIGHT_MODE, false);
            if(nightMode) {
                card.setBackgroundResourceId(R.color.colorPrimaryDark);
            }
            else if(!nightMode) {
                card.setBackgroundResourceId(R.drawable.card_background);
            }

            card.setId(String.valueOf(decks.get(i).getId()));
            card.setDeck(decks.get(i));
            mCardsList.add(card);
        }

        CardArrayAdapter mCardAdapter = new CardArrayAdapter(getActivity(), mCardsList);
        mCardAdapter.setEnableUndo(true);
        mDecksList.setAdapter(mCardAdapter);
    }
}
