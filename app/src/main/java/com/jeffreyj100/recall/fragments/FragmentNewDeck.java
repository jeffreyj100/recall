package com.jeffreyj100.recall.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.jeffreyj100.recall.MainActivity;
import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.Utils;
import com.jeffreyj100.recall.cardStructures.Deck;
import com.jeffreyj100.recall.database.MyDbHelper;

public class FragmentNewDeck extends Fragment {
    // Instance variables used by this Fragment
    private Button addDeckButton;
    private EditText deckNameEditText;
    private Bundle bundle;

    // Constructors
    public FragmentNewDeck() { }

    public static FragmentNewDeck newInstance() {
        return new FragmentNewDeck();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_new_deck, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        bundle = getArguments();

        addDeckButton = (Button) view.findViewById(R.id.submitNewDeck);
        deckNameEditText = (EditText) view.findViewById(R.id.deckTitle);
        addDeckButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (v.getId() == addDeckButton.getId()) {
                    MyDbHelper dbHelper = new MyDbHelper(getActivity());
                    Deck deck = new Deck();

                    if (!deckNameEditText.getText().toString().isEmpty()) {
                        deck.setTitle(deckNameEditText.getText().toString());
                        try {
                            dbHelper.createDeck(deck, bundle.getLong(MyDbHelper._ID));
                            Utils.hideKeyboard(getActivity());

                            // Switch to DecksFragment
                            clearBackStack();
                            ((MainActivity) getActivity()).displayView(MainActivity.DECKS_FRAG, bundle);
                        } catch (Exception e) {
                            Log.i("logging", "Could not create Deck = " + e);
                        }
                    } else {
                        Toast.makeText(getActivity(), "Deck must have a title", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }

    // Clear back stack to avoid back stack bugs
    private void clearBackStack() {
        FragmentManager manager = getFragmentManager();

        for(int i = 0; i < manager.getBackStackEntryCount(); i++) {
            manager.popBackStack();
        }
    }
}
