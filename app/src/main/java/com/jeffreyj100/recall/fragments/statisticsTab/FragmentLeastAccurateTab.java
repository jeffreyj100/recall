package com.jeffreyj100.recall.fragments.statisticsTab;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.database.MyDbHelper;
import com.jeffreyj100.recall.deckStatistics.DeckStatistic;
import com.jeffreyj100.recall.fragments.FragmentStatistics;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FragmentLeastAccurateTab extends Fragment {

    // Constructor
    public FragmentLeastAccurateTab() { }

    public static FragmentLeastAccurateTab newInstance() {
        return new FragmentLeastAccurateTab();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate layout for this fragment
        return inflater.inflate(R.layout.fragment_least_accurate_tab, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout leastAccurateLinearLayout = (LinearLayout) view.findViewById(R.id.leastAccurateRelativeLayout);

        // Get all deck statistics
        MyDbHelper dbHelper = new MyDbHelper(getActivity());
        List<DeckStatistic> deckStatisticList = dbHelper.getAllDeckStatistics();

        // Least accurate
        List<DeckStatistic> deckStatsSortedByLowestScore = getLowestScores(deckStatisticList);

        for(int i = 0; i < (deckStatsSortedByLowestScore.size() < FragmentStatistics.MAX_ITEMS ? deckStatsSortedByLowestScore.size() : FragmentStatistics.MAX_ITEMS); i++) {
            TextView statTextView = new TextView(getContext());
            int formattedScore = (int) (deckStatsSortedByLowestScore.get(i).getDeckScore() * 100);

            statTextView.setText((i + 1) + ".  " + deckStatsSortedByLowestScore.get(i).getDeckName() + "  -  " + formattedScore + "%");
            statTextView.setTextSize(18);
            statTextView.setPadding(16, 16, 16, 16);
            statTextView.setGravity(Gravity.CENTER_VERTICAL);
            leastAccurateLinearLayout.addView(statTextView);
        }
    }

    private List<DeckStatistic> getLowestScores(List<DeckStatistic> deckStatisticList) {
        Collections.sort(deckStatisticList, new SmallestScoreComparator());

        return deckStatisticList;
    }

    private class SmallestScoreComparator implements Comparator<DeckStatistic> {
        @Override
        public int compare(DeckStatistic lhs, DeckStatistic rhs) {
            if(lhs.getDeckScore() < rhs.getDeckScore()) {
                return -1;
            } else if(lhs.getDeckScore() > rhs.getDeckScore()) {
                return 1;
            }
            return 0;
        }
    }
}
