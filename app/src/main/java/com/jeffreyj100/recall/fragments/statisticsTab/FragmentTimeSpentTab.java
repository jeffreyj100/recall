package com.jeffreyj100.recall.fragments.statisticsTab;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.jeffreyj100.recall.R;
import com.jeffreyj100.recall.database.MyDbHelper;
import com.jeffreyj100.recall.deckStatistics.DeckStatistic;
import com.jeffreyj100.recall.fragments.FragmentStatistics;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class FragmentTimeSpentTab extends Fragment {

    // Constructor
    public FragmentTimeSpentTab() { }

    public static FragmentTimeSpentTab newInstance() {
        return new FragmentTimeSpentTab();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) { super.onCreate(savedInstanceState); }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate layout for this fragment
        return inflater.inflate(R.layout.fragment_time_spent_tab, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        LinearLayout leastAccurateLinearLayout = (LinearLayout) view.findViewById(R.id.timeSpentRelativeLayout);

        // Get all deck statistics
        MyDbHelper dbHelper = new MyDbHelper(getActivity());
        List<DeckStatistic> deckStatisticList = dbHelper.getAllDeckStatistics();

        // Least accurate
        List<DeckStatistic> deckStatsSortedByTimeSpent = getMostTimeSpent(deckStatisticList);

        for(int i = 0; i < (deckStatsSortedByTimeSpent.size() < FragmentStatistics.MAX_ITEMS ? deckStatsSortedByTimeSpent.size() : FragmentStatistics.MAX_ITEMS); i++) {
            TextView statTextView = new TextView(getContext());
            int formattedTime = (int) (deckStatsSortedByTimeSpent.get(i).getTimeSpent());
            int minutes = formattedTime / 60;
            int seconds = formattedTime % 60;
            statTextView.setText((i + 1) + ".  " + deckStatsSortedByTimeSpent.get(i).getDeckName() + "  -  " + minutes + " min " + seconds + " sec");
            statTextView.setTextSize(18);
            statTextView.setPadding(16, 16, 16, 16);
            statTextView.setGravity(Gravity.CENTER_VERTICAL);
            leastAccurateLinearLayout.addView(statTextView);
        }
    }

    private List<DeckStatistic> getMostTimeSpent(List<DeckStatistic> deckStatistics) {
        Collections.sort(deckStatistics, new LargerTimeSpentComparator());

        return deckStatistics;
    }

    private class LargerTimeSpentComparator implements Comparator<DeckStatistic> {
        @Override
        public int compare(DeckStatistic lhs, DeckStatistic rhs) {
            if(lhs.getTimeSpent() > rhs.getTimeSpent()) {
                return -1;
            } else if(lhs.getTimeSpent() < rhs.getTimeSpent()) {
                return 1;
            }
            return 0;
        }
    }
}
