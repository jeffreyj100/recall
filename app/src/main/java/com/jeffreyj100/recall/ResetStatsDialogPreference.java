package com.jeffreyj100.recall;

import android.content.Context;
import android.content.DialogInterface;
import android.preference.DialogPreference;
import android.util.AttributeSet;
import android.widget.Toast;

import com.jeffreyj100.recall.database.MyDbHelper;

public class ResetStatsDialogPreference extends DialogPreference {

    public ResetStatsDialogPreference(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void onClick(DialogInterface dialogInterface, int option) {
        int CONFIRM_RESET = -1;

        if(option == CONFIRM_RESET) { // Delete all deck stats from db
            MyDbHelper dbHelper = new MyDbHelper(getContext());
            dbHelper.deleteAllEntriesForTable(MyDbHelper.DECK_STATISTICS_TABLE);
            Toast.makeText(getContext(), "Deck statistics cleared", Toast.LENGTH_SHORT).show();
        }
    }
}
