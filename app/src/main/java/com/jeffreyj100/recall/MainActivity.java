package com.jeffreyj100.recall;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;

import com.jeffreyj100.recall.fragments.FragmentDecks;
import com.jeffreyj100.recall.fragments.FragmentFlashcardViewer;
import com.jeffreyj100.recall.fragments.FragmentFlashcardsList;
import com.jeffreyj100.recall.fragments.FragmentNewDeck;
import com.jeffreyj100.recall.fragments.FragmentNewFlashcard;
import com.jeffreyj100.recall.fragments.FragmentNewSubject;
import com.jeffreyj100.recall.fragments.FragmentSubjects;

public class MainActivity extends NavigationDrawerActivity {
    // Fragment position constants
    public static final int FLASHCARDS_VIEWER = -6;
    public static final int FLASHCARDS_FRAG = -5;
    public static final int DECKS_FRAG = -4;
    public static final int NEW_FLASHCARD_FRAG = -3;
    public static final int NEW_DECK_FRAG = -2;
    public static final int NEW_SUBJECT_FRAG = -1;
    public static final int SUBJECTS_FRAG = 0;

    public static FragmentManager fm;

    @Override
    public void displayView(int position, Bundle fragmentBundle) {
        fm = getSupportFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        Fragment fragmentPicked = null;
        Class fragmentClassPicked = null;

        switch(position) {
            case FLASHCARDS_FRAG:
                fragmentPicked = FragmentFlashcardsList.newInstance();
                fragmentClassPicked = FragmentFlashcardsList.class;
                break;
            case DECKS_FRAG:
                fragmentPicked = FragmentDecks.newInstance();
                fragmentClassPicked = FragmentDecks.class;
                break;
            case NEW_FLASHCARD_FRAG:
                fragmentPicked = FragmentNewFlashcard.newInstance();
                fragmentClassPicked = FragmentNewFlashcard.class;
                break;
            case NEW_DECK_FRAG:
                fragmentPicked = FragmentNewDeck.newInstance();
                fragmentClassPicked = FragmentNewDeck.class;
                break;
            case NEW_SUBJECT_FRAG:
                fragmentPicked = FragmentNewSubject.newInstance();
                fragmentClassPicked = FragmentNewSubject.class;
                break;
            case SUBJECTS_FRAG:
                fragmentPicked = FragmentSubjects.newInstance();
                fragmentClassPicked = FragmentSubjects.class;
                break;
            case FLASHCARDS_VIEWER:
                fragmentPicked = FragmentFlashcardViewer.newInstance();
                fragmentClassPicked = FragmentFlashcardViewer.class;
                break;
            default:
                break;
        }

        if(fragmentPicked != null) {
            if(fragmentBundle != null) {
                fragmentPicked.setArguments(fragmentBundle);
            }

            ft.setCustomAnimations(R.anim.enter_right, R.anim.exit_left, R.anim.enter_left, R.anim.exit_right);
            ft.replace(R.id.frame_content, fragmentPicked);

            if(!(fragmentPicked instanceof FragmentSubjects)) {
                ft.addToBackStack(String.valueOf(fragmentClassPicked.getCanonicalName()));
            }
            ft.commit();
        } else {
            Log.i("logging", "Error creating fragment");
        }
    }
}
